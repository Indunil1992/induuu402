const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const Swagger = require('swagger-client');

exports.handler = async (event) => {
    try {
        let response = await Swagger.http({
            url: `https://www.afterbanks.com/forms`,
            method: 'get',
            query: { "country_code": "20000" },
            headers: { "Accept": "application/json" }
        });
        // your code goes here

    } catch (err) {
        // error handling goes here
    }
    try {
        let data = await s3.listObjects({
            Bucket: "indufintech402",
            MaxKeys: 10
        }).promise();

    } catch (err) {
        // error handling goes here
    };

    return { "message": "Successfully executed" };
};